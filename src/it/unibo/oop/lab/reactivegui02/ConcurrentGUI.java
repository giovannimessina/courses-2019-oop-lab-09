package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * 
 * Class that create Simple GUI.
 * 
 */
public class ConcurrentGUI extends JFrame {
	
	private final JButton up = new JButton("UP");
	private final JButton down = new JButton("DOWN");
	private final JButton stop = new JButton("STOP");
	private final JLabel lab = new JLabel();
	
	/**
	 * 
	 */
	public ConcurrentGUI() {
		super("MY FIRST COUNTER");
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		super.setContentPane(panel);
		panel.add(lab);
		panel.add(up);
		panel.add(down);
		panel.add(stop);
		//this.setResizable(false);
		this.setVisible(true);
		this.pack();

		Counter count = new Counter();
		count.incrementCounter();
		new Thread(count).start();
		up.addActionListener(new ActionListener() {


			public void actionPerformed(final ActionEvent e) {
				count.incrementCounter();
			}
		});

		down.addActionListener(new ActionListener() {

			public void actionPerformed(final ActionEvent e) {
				count.decrementCounter();
			}

		});
		
		stop.addActionListener(new ActionListener() {

			public void actionPerformed(final ActionEvent e) {
				count.stopCounter();			
			}
		});
		}
	
	private class Counter implements Runnable {

		private volatile int count = 0;
		private volatile boolean increment = true;
		private volatile boolean stopFlag;
		
		public void run() {
			while (!stopFlag) {
				try {
					SwingUtilities.invokeAndWait(new Runnable() {

						public void run() {
							lab.setText(Integer.toString(count));
						}
					});
					Thread.sleep(1000);
					if (this.increment) {
						count++;
						} else {
							count--;
						}
					

				} catch (InvocationTargetException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
			private void incrementCounter() {
				this.increment = true;
				up.setEnabled(false);
				down.setEnabled(true);
				
			}
			private void decrementCounter() {
				this.increment = false;
				down.setEnabled(false);
				up.setEnabled(true);
			}
			private void stopCounter() {
				this.stopFlag = true;
				down.setEnabled(false);
				up.setEnabled(false);
				stop.setEnabled(false);
				if(Integer.parseInt(lab.getText()) == 0) {
					JOptionPane winWindow = new JOptionPane();
					winWindow.showMessageDialog(new JPanel(), "YOU WON!!");
				}
			}
	}
}
